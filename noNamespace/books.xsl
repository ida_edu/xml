<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>
    <xsl:template match="/books">
        <html>
            <head>
                <title>Список книг</title>
                <style>
                    table { border: 1px solid; border-collapse: collapse; }
                    th { border: 1px solid; background-color: lightgray; padding: 10px; }
                    td { border: 1px solid; vertical-align: top; padding: 10px; }
                    ul { padding-left: 25px; }
                </style>
            </head>
            <body>
                <h1>Пример списка книг</h1>
                <table>
                    <tr>
                        <th>№</th>
                        <th>Название</th>
                        <th>Авторы</th>
                        <th>Кол-во страниц</th>
                    </tr>
                    <xsl:for-each select="book">
                        <tr>
                            <td><xsl:value-of select="position()"/></td>
                            <td><xsl:value-of select="title"/></td>
                            <td>
                                <xsl:choose>
                                    <xsl:when test="authors">
                                        <ul>
                                            <xsl:for-each select="authors/author">
                                                <li><xsl:value-of select="."/></li>
                                            </xsl:for-each>
                                        </ul>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        Авторы не указаны
                                    </xsl:otherwise>
                                </xsl:choose>
                            </td>
                            <td><xsl:value-of select="pages"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
